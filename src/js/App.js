var React = require('react');
var ReactDOM = require('react-dom');
var NavWrapper = require('./components/NavWrapper');
var cookieData = parseCookie('dlc_whoami');

sessionStorage.env = process.env.env;
sessionStorage.user = JSON.stringify({dasher_user_id: cookieData.uid});
var e = sessionStorage.env ? sessionStorage.env : 'prod';
var u = sessionStorage.user ? JSON.parse(sessionStorage.user) : {};
window.App = {
	environment: e,
	user: u,

	supportApi: function() {
		switch (window.App.environment) {
			case 'test':
				return 'testsupportapi/';
			case 'stage':
				return 'stagesupportapi/';
			case 'local':
				return 'localsupportapi/';
			default:
				return 'prodsupportapi/';
		}
	},

	clientApi: function() {
		switch (window.App.environment) {
			case 'test':
				return 'testclientapi/';
			case 'stage':
				return 'stageclientapi/';
			case 'local':
				return 'localclientapi/';
			default:
				return 'prodclientapi/';
		}
	},

  ajax: function(apiPath, success, failure, options) {
    options = options ? options : {};
    console.log('Fetch: ', apiPath);

    if (apiPath.match('mapping')) // TODO
      return success(TEST_DATA);

    return $.ajax({
      url: apiPath,
      type: options.method || 'GET',
      data: options.data,
      contentType: 'application/json',
      success: success,
      error: failure
    });
  },

	logout: function() {
		function deleteCookie(name) {
		  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		}
		deleteCookie('dlc_1337');
		deleteCookie('dlc_whoami');
		location.href='/';
	},

	utcToLocal: function(utcTime) {
		if (!utcTime.match(/Z$/)) {
			utcTime = utcTime + ' Z';
		}
		var d = new Date(utcTime);
		return d.toString().replace(/\sGMT-\d\d\d\d/, '');
	},

	notify: function() {
		// Overload this function
	},

	warn: function() {
		// Overload this function
	},

	closeMessage: function() {
		// Overload this funciton
	}
};


function parseCookie(cName) {
	var cookies = document.cookie.split(';');
	var ret = {};
	for (var c=0; c<cookies.length; c++) {
		var cookie = cookies[c];
		if (cookie.split('=')[0].trim() === cName) {
			var data = cookie.split('=');
			for (var d=0; d<data.length; d+=2) {
				try {
					return JSON.parse(decodeURIComponent(data[d+1]));
				} catch(err) {
					console.error(err);
				}
			}
		}
	}
	return ret;
}


$(document).ajaxError((event, jqxhr, settings, thrownError) => {
	if (thrownError === 'Unauthorized') {
		if (jqxhr.responseJSON && jqxhr.responseJSON.error_code) { // Server auth error
			return App.warn(
				'<h5>'+jqxhr.responseJSON.error_code+'</h5>'+
				'<p>'+jqxhr.responseJSON.message+'</p>'
			);
		}
		App.warn(
			'<span><p>You have been logged out due to inactivity.</p>'+
			'<p>Please refresh the page to log in.</p></span>'
		);
	}
});

ReactDOM.render(<NavWrapper />, document.getElementById('app'));
