var React = require('react');
var Router = require('react-router').Router;
var Route = require('react-router').Route;
var hashHistory = require('react-router').hashHistory;
var Header = require('./Header');
var Messaging = require('./Messaging');

var Templates = require('./template/Templates');
var Dashboard = require('./campaign/Dashboard');

var NavWrapper = React.createClass({

	menuOptions: [
    {
    	name: 'campaigns',
    	path: '/'
    },
    {
    	name: 'templates',
    	path: '/templates'
    }
  ],


  changeView: function(newPath) {
  	hashHistory.push(newPath);
  },


  render: function() {
  	var that = this;
  	var HeaderWrapper = React.createClass({
			render: function() {
			  return <div>
	  			<Header menuOptions={ that.menuOptions } changeView={ that.changeView } children={ this.props.children } />
		  		<Messaging />
		  	</div>
			}
		});

    return (
      <div className="wrapper">
    		<Router history={hashHistory}>
    			<Route component={HeaderWrapper}>

	        	<Route path="/" component={Dashboard} />
	        	<Route path="/templates" component={Templates} />
	        </Route>
        </Router>
      </div>
    );
  }
});

module.exports = NavWrapper;
