var React = require('react');
var TemplateFields = require('./TemplateFields');
// var TemplateChooser = require('./TemplateChooser');
var TemplateTile = require('./../common/TemplateTile');
var ActionStateInstructions = require('./ActionStateInstructions');

var Templates = React.createClass({

  clientPath: function() {
    return App.clientApi() + 'v1/';
  },


  supportPath: function() {
    return App.supportApi() + 'v1/';
  },


  getInitialState: function() {
    return {
      action: 'pick',
      chosenTemplate: null, 
      fieldMapping: {},
      config: {},
      allTemplates: [],
      templateJson: {}
    };
  },


  componentWillMount: function() {
    this.resetView();
  },


  resetView: function() {
    this.setState({action:'pick'});
    this.fetchEverything();
  },


  fetchEverything: function() {
    var that = this;

    // Fetch template field mapping JSON
    var path = this.clientPath() + 'mapping';
    App.ajax(path, 
      function success(mapping) {
        that.setState({fieldMapping: mapping});
      },
      function failure(err) {
        console.error(err);
        App.warn('Unable to fetch field mapping.');
      }
    );

    // Fetch list of existing template
    path = this.supportPath() + 'marketing/templates';
    App.ajax(path,
      function success(templates) {
        that.setState({allTemplates: templates || []});
      },
      function failure(err) {
        console.error(err);
        App.warn('Unable to fetch existing templates.');
      }
    );

    // Fetch list of existing template
    path = this.supportPath() + 'marketing/config';
    App.ajax(path,
      function success(templates) {
        that.setState({config: templates || []});
      },
      function failure(err) {
        console.error(err);
        App.warn('Unable to fetch config.');
      }
    );
  },


  renderSubstitutionMarkers: function() {
    var config = this.state.config;
    var markers = [];
    if ($.isEmptyObject(config) || !config.substitution_markers) {
      return (<span />);
    }
    for (var s=0; s<config.substitution_markers.length; s++) {
      var marker = config.substitution_markers[s];
      markers.push(<li key={s} title={marker.description || 'No Description'}>{marker.type}</li>);
    }
    return markers;
  },


  renderActionStateInstructions: function() {
    if (!this.state.actionStateInstructions) {
      return <div>...</div>;
    }


  },


  renderEditForm: function() {
    var existing = null;
    var chosenId = this.state.chosenTemplate;
    var delButton = (<div />);
    if (chosenId) {
      existing = this.state.allTemplates.find(function(template){
        return template.template_id == chosenId;
      }, this);
      existing = this.appendTopLevelFields(existing);
      delButton = (<div className="button delete" onClick={ this.deleteTemplate }>Delete Template</div>);
    }

    if (!this.state.fieldMapping) {
      return (<section><p>Loading...</p></section>);
    }

    return (
      <section>
        <p>Use this form to save a marketing campaign template.</p>
        <div className={ 'banner' + (chosenId ? ' alert' : '') }>
          { chosenId ? 'Editing Existing Template (ID: '+chosenId+')' : 'Editing New Template' }
        </div>
        <div className="form">
          <TemplateFields
            fieldMapping={ this.state.fieldMapping }
            existingTemplate = { existing }
            handleUpdateField={this.updateField} />
          <br />
          <div className="button" onClick={ this.saveTemplate }>
            { chosenId ? 'Update Template' : 'Save Template' }
          </div>
          { delButton }
        </div>
        
        <div className="instructions">
          <h3>Instructions</h3>
          <div className="block-title">Substitution Markers:</div>
          <div className="box-wrapper short">
            <ul>
              { this.renderSubstitutionMarkers() }
            </ul>
          </div>

          <div className="block-title">Action_State / State_Parms:</div>
          <div className="box-wrapper">
            <ActionStateInstructions />
          </div>          
        </div>
      </section>
    );
  },


  // renderTemplatePicker: function() {
  //   return ( <TemplateChooser handleChoice={this.chooseTemplate} allTemplates={this.state.allTemplates} />);
  // },


  renderTemplateList: function() {
    var that = this;
    function addTemplate(template, idx) {
      return (
        <TemplateTile key={idx} className="wizard-option" template={template} onClick={function(){
            that.chooseTemplate({target:{value:template.template_id}})
          }} />
      );
    }

    var list = this.state.allTemplates.map(addTemplate);

    return (
      <ul className="wizard-options">
        { list }
      </ul>
    );
  },


  render: function() {
    var that = this;
    var action = this.state.action;
    var templateForm = action === 'create' ? this.renderEditForm() : (<div />);
    // var chooseTemplate = action === 'edit' ? this.renderTemplatePicker() : (<div />);
    var templateList = action === 'pick' ? this.renderTemplateList() : (<div />);
    var pickButtonClass = "action button " + (action === 'pick' ? '' : 'hide');
    var editButtonClass = "action button navigate " + (action === 'pick' ? 'hide' : '');

    return ( 
      <section className="template-wrapper">
        <div className="sections-header">
          <div className={pickButtonClass} onClick={function(){that.setState({action:'create',chosenTemplate:null})}}>
            Create New Template
          </div>
          <div className={editButtonClass} onClick={function(){that.setState({action:'pick'})}}>
            Back to List
          </div>
          <h4>Create/Update Marketing Templates</h4>
        </div>
        <div className="sections">
          { templateList }
          { templateForm }
        </div>
      </section>
    );
  },


  chooseTemplate: function(evt) {
    var templateId = evt.target.value;
    this.setState({
      chosenTemplate: templateId,
      action: 'create'
    });
  },


  updateField: function(field) {
    var tj = this.state.templateJson;
    for (var key in field) {
      tj[key] = field[key];
    }
    this.setState({templateJson: tj});
  },


  appendTopLevelFields: function(templateObj) {
    // Copy top-level fields down into their form fields
    templateObj.display_json.enabled = true;
    templateObj.display_json.enabled = true;
    templateObj.display_json.template_name = templateObj.template_name;
    templateObj.display_json.feed_item_type = templateObj.category;
    templateObj.display_json.type = templateObj.type;
    return templateObj;
  },


  saveTemplate: function() {
    var that = this;

    // Validate fields
    var invalid = false;
    $(".output:visible").each(function(idx) {
      var required = $(this).parents('.labeled-input').find('label.required').length != 0;
      if (required && !this.value) {
        invalid = this.name;
        return false;
      }
      else if (required && this.type == 'radio') { // Check all in radio group

        if (! $('input[type="radio"][name="'+this.name+'"]:checked').length) {
          invalid = this.name;
          return false;
        }
      }

      if (that.state.fieldMapping.textAreas.indexOf(this.name) >= 0) {
        try {
          JSON.parse(this.value);
        } catch(err) {
          invalid = 'Invalid JSON in ' + this.name;
          return false;
        }
      }
    });
    if (invalid) {
      return App.warn("Please fill in all required fields! <br/> "+invalid);
    }

    var data = {
      template_name: this.state.templateJson.template_name,
      category: this.state.templateJson.feed_item_type,
      type: this.state.templateJson.type,
      enabled: true, // server can't handle this not being set and if we are editing the template then it has to be enabled
      display_json: this.state.templateJson
    }

    // POST the template to the server
    var path = this.supportPath() + 'marketing/templates';
    if (this.state.chosenTemplate) {
      path += '/'+this.state.chosenTemplate;
      var method = 'PUT';
    }
    App.ajax(path, 
      function success(resp) {
        console.log('SAVE', resp);
        App.notify('Template saved successfully.');
        that.resetView();
      },
      function failure(err) {
        console.error(err);
        App.warn('Unable to save template.');
        that.resetView();
      },
      {
        method: method || 'POST',
        data: JSON.stringify(data)
      }
    );
  },


  deleteTemplate: function() {
    if (!window.confirm('Comfirm delete?')) {return;}
    var that = this;

    var path = this.supportPath() + 'marketing/templates';
    if (this.state.chosenTemplate) {
      path += '/'+this.state.chosenTemplate;
    }
    else {
      return; // Shouldn't be able to happen...
    }
    App.ajax(path, 
      function success(resp) {
        console.log('DELETE', resp);
        App.notify('Template deleted successfully.');
        that.resetView();
      },
      function failure(err) {
        console.error(err);
        App.warn('Unable to delete template.');
        that.resetView();
      },
      { method: 'DELETE' }
    );
  }
});

module.exports = Templates;
