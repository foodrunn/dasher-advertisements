var React = require('react');

var TemplateChooser = React.createClass({

  getInitialState: function() {
    return {
      conditionals: {},
    };
  },


  render: function() {
    var options = [];
    var disabledOpts = [];
    function addTemplate(template, idx) {
      var disabled = template.enabled == false;
      if (disabled)
        disabledOpts.push( (<option key={idx} value={template.template_id}>{template.template_name || 'NO NAME'}</option>) );
      else
        options.push( (<option key={idx} value={template.template_id}>{template.template_name || 'NO NAME'}</option>) );
    }
    this.props.allTemplates.map(addTemplate);

    return (
      <section className="form">
        <p>Choose a template</p>
          <div className="select">
            <select onChange={this.props.handleChoice} name="all-template">
              <option value=''>Choose...</option>
              <optgroup label="Enabled">{ options }</optgroup>
              <optgroup label="Disabled">{ disabledOpts }</optgroup>
            </select>
          </div>
      </section>
    );
  }
});

module.exports = TemplateChooser;
