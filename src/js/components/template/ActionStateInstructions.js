var React = require('react');

var ActionStateInstructions = React.createClass({

  getInitialState: function() {
    return {
      data: TEST_DATA.instructions.actionStateInstructions
    };
  },


  renderItem: function(item, i) {
    let body = this.renderBody(item.description);
    let params = this.renderParams(item.params);
    return (
      <div key={i}>
        <div className="title">{ item.title }</div>
        <div className="description">{ body }</div>
        { params }
      </div>
    );
  },


  renderBody(description) {
    if (typeof description === 'string') {
      return description;
    }

    if (Array.isArray(description)) {
      return description.map( (p,i) => <p key={i}>{ p }</p> );
    }
  },


  renderParams: function(params) {
    if (!params || !params.length) {
      return (<div></div>);
    }

    let items = params.map( (param, i) => {
      return ( <li key={i}><b>{ param.name }</b> &mdash; { param.description }</li> );
    });

    return (
      <div className="params">
        <div><b><i>state_params</i></b></div>
        <ul> { items } </ul>
      </div>
    );
  },


  renderInstuctions: function() {
    return this.state.data.map( (item, i) => {
      return this.renderItem(item, i);
    });
  },


  render: function() {
    return (
      <section className="action-state-instructions">
        { this.renderInstuctions() }
      </section>
    );
  }

});

module.exports = ActionStateInstructions;

