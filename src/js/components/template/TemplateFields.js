var React = require('react');
var TextInput = require('../common/TextInput');
var SelectInput = require('../common/SelectInput');
var TextareaInput = require('../common/TextareaInput');
var BooleanInput = require('../common/BooleanInput');
var ImageModal = require('../common/ImageModal');

var TemplateFields = React.createClass({

  previewImage: '',
  getInitialState: function() {
    return {
      previewOpen: false,
      conditionals: {},
      formData: this.resetForm()
    };
  },


  componentDidMount: function() {
    $($("input:text")[0]).focus(() => { $(this).select(); } );
    $('.focus-me').focus();
    this.renderExisting();
  },


  componentWillReceiveProps: function(nextProps) {
    if (this.props.existingTemplate && !nextProps.existingTemplate) {
      this.setState({formData: this.resetForm()});
    }
  },


  updateFormData: function(fieldName, value, callback) {
    var newFormData = this.state.formData;
    newFormData[fieldName] = value;
    this.setState({formData: newFormData}, callback);
  },


  resetForm: function() {
    var newFormData = {};
    var allFields = this.props.fieldMapping.templateFields.all;
    
    allFields.map( (fieldName) => {
      if (typeof fieldName == 'object') {
        var fn = Object.keys(fieldName)[0];
        newFormData[fn] = '';
      }
      else {
        newFormData[fieldName] = '';
      }
    });
    return newFormData;
  },


  renderExisting: function() {
    var existing = this.props.existingTemplate;
    if (!existing) {
      return;
    }

    for (var fieldName in existing.display_json) {
      var value = existing.display_json[fieldName];

      if (typeof value == 'object') {
        value = JSON.stringify(value);
      }

      var htmlField = $('.output[name="'+fieldName+'"]');
      if (!htmlField.length) {
        continue; // This field will be conditionally added to the form as required
      }
      // Siumulate what happens when a user inputs a new value
      htmlField.val(value);
      this.setConditional({target: htmlField[0]}); // Some fields have conditional fields, render them
    }
  },


  renderInputs: function(field, idx) {
    var label;
    var that = this;
    var enums = this.props.fieldMapping.templateEnums;
    var isRequired = this.props.fieldMapping.requiredFields.indexOf(field) >= 0;
    
    // Handle fields that are only required if another field is set
    if (!isRequired) {
      var dependencyRequired = this.props.fieldMapping.dependentRequirements[field];
      if (dependencyRequired && this.state.formData[dependencyRequired]) {
        isRequired = true;
      }
    }

    var instructionLink = this.props.fieldMapping.instructionLinks[field];
    var conditionals = (<div className="no-conditionals"></div>);
    if (typeof(field) === 'object') {
      var key = Object.keys(field)[0];

      // trying to get lable for the field
      if (field[key]['label'] !== undefined) {
        label = field[key]['label'];
      }
      
      isRequired = this.props.fieldMapping.requiredFields.indexOf(key) >= 0;
      conditionals = this.renderConditional(field, key, idx);
      field = key;
    }

    // Known "type"
    var isEnum = enums[field];
    if (isEnum) {

      if (typeof(isEnum) === 'string') {
        switch(isEnum) {
          case "boolean":
            return returnFields(idx, booleanInput(field, isRequired)); // Radio
            break;
          default:
            return returnFields(idx, textInput(field, isRequired, label)); // Text
        }
      }

      // Dropdown (Array)
      else if (Array.isArray(isEnum)) {
        return returnFields(idx, selectInput(field, isEnum, isRequired, label));
      }

      // Field based on another key in the form (Object)
      else if ($.isPlainObject(isEnum)) {
        // Assumes only one key allowed in object - represents another top-level form key
        var formKey = Object.keys(isEnum)[0];
        var formVal = this.state.formData[formKey];
        var fieldArray = isEnum[formKey][formVal];
        if (!fieldArray) {
          return; // Haven't set a value for formKey yet
        }

        // Text field
        if (typeof(fieldArray) === 'string') {         
          return returnFields(idx, textInput(field, isRequired, label));
        }
        // Dropdown
        else {
          return returnFields(idx, selectInput(field, fieldArray, isRequired, label));
        }
      }
    }

    // Default to text input
    else {
      if (this.props.fieldMapping.textAreas.indexOf(field) >= 0) {
        return returnFields(idx, textAreaInput(field, isRequired));
      }
      return returnFields(idx, textInput(field, isRequired, label));
    }

    function returnFields(key, field) {
      return (
        <div key={key} className="field-group">
          { field }
          { conditionals }
        </div>
      );
    }

    function textInput(fieldName, required, label) {
      var isUpload = that.props.fieldMapping.s3Assets.indexOf(field) >= 0;
      
      var formValue = that.state.formData[fieldName];
      if (isUpload) {
        that.previewImage = formValue;
      }
      return ( <TextInput
        fieldName={fieldName}
        label={label}
        required={required}
        isUpload={isUpload}
        value={formValue}
        uploadWidth={900}
        uploadHeight={[160,230]}
        maxSize={600000}
        link={instructionLink}
        handlePreviewClick={that.toggleImageModal}
        handleChange={that.updateField}
        handlePreview={that.updatePreview} />);
    }

    function textAreaInput(fieldName, required) {
      return (<TextareaInput
        fieldName={fieldName}
        value={that.state.formData[fieldName]}
        required={required} 
        handleChange={that.updateField} />);
    }

    function selectInput(fieldName, opts, required, label) {
      return (<SelectInput
        handleChange={that.setConditional}
        fieldName={fieldName}
        value={that.state.formData[fieldName]}
        opts={opts}
        label={label}
        required={required} />);
    }

    function booleanInput(fieldName, required) {
      return (<BooleanInput 
        handleChange={that.setConditional} 
        fieldName={fieldName}
        value={that.state.formData[fieldName]}
        required={required}/>);
    }
  },


  updateField: function(evt) {
    this.updateFormData(evt.target.name, $(evt.target).val(), () => {
      this.props.handleUpdateField(this.state.formData);
    });
  },


  updatePreview: function(evt) {
    var imgSrc = $(evt.target).val();
    $('.preview img').attr('src', imgSrc);
    this.previewImage = imgSrc;
  },


  toggleImageModal: function() {
    this.setState({previewOpen: !this.state.previewOpen});
  },


  setConditional: function(evt) {
    var cond = this.state.conditionals;
    var conditionAlreadyChecked = cond[evt.target.name] != undefined;
    cond[evt.target.name] = evt.target.value;
    this.setState({conditionals: cond}, () => {
      if (!conditionAlreadyChecked) {
        this.renderExisting(); // Happens only when the condition hasn't been checked already
      }
    });
    this.updateField(evt);
  },


  renderConditional: function(object, key, idx) {
    var that = this;
    var val = this.state.conditionals[key];
    if (!val) {
      return (<div key={idx} className="_no-conditional" />);
    }

    if (object[key] && object[key][val]) {
      return object[key][val].map(function(field, idx) {
        return that.renderInputs(field, idx);
      });
    }
    else { // Shouldn't happen
      return (<div key={idx} className="_no-conditional" />);
    }
  },


  renderFields: function(fields) {
    var fields = fields.all.map(this.renderInputs);

    return (
      <div key={0}>
        {fields}
        <ImageModal
          active={this.state.previewOpen}
          src={this.previewImage}
          handleCloseModal={this.toggleImageModal} />
      </div>
    );
  },


  render: function() {
    var mapping = this.props.fieldMapping;
    var fields = (<div />);
    if (mapping) {
      fields = this.renderFields(mapping.templateFields);
    }

    return fields;
  }
});

module.exports = TemplateFields;
