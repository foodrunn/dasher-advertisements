var React = require('react');

var ImageModal = React.createClass({

  render: function() {
    var modalStyle = {
      display: this.props.active ? 'block' : 'none',
      position: "fixed",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      backgroundColor: "rgba(0,0,0,0.8)",
      cursor: "pointer"
    };

    var modalClose = {
      color: "#FFF",
      fontSize: "x-large",
      position: "absolute",
      top: "10px",
      right: "15px",
      cursor: "pointer"
    };

    var modalContent = {
      display: "block",
      maxWidth: "80%",
      maxHeight: "80%",
      margin: "0 auto",
      marginTop: "5%",
      cursor: "auto"
    };

    return ( 
      <div style={modalStyle} className="image-modal"  onClick={this.close}>
        <div style={modalClose}>&#10005;</div>
        <img style={modalContent} src={this.props.src} />
      </div>
    );
  },

  close: function(evt) {
    if (evt.target.nodeName == 'IMG') {
      return;
    }
    this.props.handleCloseModal(evt);
  }
});

module.exports = ImageModal;
