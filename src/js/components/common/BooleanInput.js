var React = require('react');
var BooleanInput = React.createClass({

  getInitialState: function() {
    return {};
  },


  render: function() {
    var value = this.props.value;

    return (
      <span className="labeled-input bool-field">
        <label className={this.props.required ? " required" : ""}>
          { this.props.fieldName }
        </label>
        <label>
          <input 
            type="radio"
            className="output"
            name={this.props.fieldName}
            value="true"
            checked={value === 'true' || value === true}
            onChange={this.props.handleChange} />
          True
        </label>
        <label>
          <input 
            type="radio"
            className="output"
            name={this.props.fieldName}
            value="false"
            checked={value === 'false' || value === false}
            onChange={this.props.handleChange} />
          False
        </label>
      </span>
    );
  }
});

module.exports = BooleanInput;
