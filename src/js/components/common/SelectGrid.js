var React = require('react');
var SelectGridOption = require('./SelectGridOption');

var component = React.createClass({

  getInitialState: function() {
    return {
      selected: []
    };
  },


  componentWillMount: function() {
    this.setState({ selected: this.props.value });
  },


  componentWillReceiveProps: function() {
    this.componentWillMount();
  },


  toggleOption: function(evt) {
    var val = Number(evt.target.dataset["value"]);

    if (val !== 0) {
      // remove 'all'
      if (this.state.selected.length === 1 && this.state.selected[0] === 0) {
        this.state.selected.splice(this.state.selected.indexOf(0), 1);
      }
      // toggle option
      if (this.state.selected.includes(val)) {
        this.state.selected.splice(this.state.selected.indexOf(val), 1);
      } else {
        this.state.selected.push(val);
      }
      // if nothing selected, add 'all'
      if (this.state.selected.length === 0) {
        this.state.selected.push(0);
      }
    } else {
      this.state.selected.splice(0, this.state.selected.length, 0);
    }
    this.setState({ selected: this.state.selected });
    this.props.handleChange(this.props.fieldName, this.state.selected);
  },


  render: function() {
    var that = this;
    var opts = this.props.opts;
    var options = [];

    if (!Array.isArray(opts)) { // Object with name and value mappings
      var arr = Object.keys(opts).map(function(opt, i) {
        return { name: opt, value: opts[opt] };
      });
      opts = arr;
    }

    if (this.props.sortBy) {
      opts = opts.sort(function(a, b) {
        if ( a[that.props.sortBy] < b[that.props.sortBy] ) { return -1; }
        if ( a[that.props.sortBy] > b[that.props.sortBy] ) { return 1; }
        return 0;
      });
    }

    if (this.props.includeAll) {
      opts.splice(0, 0, {name: 'All', value: 0});
    }

    options = opts.map(function(opt, i) {
      var css = 'select-grid-option ' + (that.props.value.includes(opt.value) ? 'selected' : '');

      return (
        <div key={i} className={css} data-value={opt.value} onClick={that.toggleOption}>
          { opt.name }
        </div>
      );  
    });

    return (
      <span className="labeled-input">
        <label className={this.props.required ? " required" : ""}>{this.props.fieldName}</label>

        <div className="select-grid">
          { options }
        </div>
      </span>
    );  
  }
});

module.exports = component;
