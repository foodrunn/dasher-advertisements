var React = require('react');

var SelectInput = React.createClass({

  getInitialState: function () {
    return {};
  },


  render: function () {
    var that = this;
    var opts = this.props.opts;

    if (!Array.isArray(opts)) { // Object with name and value mappings
      var options = Object.keys(opts).map(function (opt, i) {
        return (<option key={i} value={opts[opt]}>{opt}</option>);
      });
    }
    else { // Array of options (same name and value)
      var options = opts.map(function (opt, i) {
        return (<option key={i} value={opt}>{opt}</option>);
      });
    }

    var multiple = this.props.multiple != null ? ' multiple' : '';
    var instruction = (<div />);
    if (multiple) {
      var btn = navigator.platform == 'MacIntel' ? 'cmd' : 'ctrl';
      instruction = (<p className="instruction">{btn}+click to select multiple</p>);
    }
    else {
      options.splice(0, 0, (<option key='nada' value=''>Choose an option</option>));
    }
    return (
      <span className="labeled-input">
        <label className={this.props.required ? " required" : ""}>{this.props.label !== undefined ? this.props.label : this.props.fieldName}</label>

        <div className={"select" + multiple}>
          <select className={"output" + multiple}
            value={this.props.value}
            multiple={this.props.multiple}
            onChange={this.props.handleChange}
            name={this.props.fieldName}>
            {options}
          </select>
          {instruction}
        </div>
      </span>
    );
  }
});

module.exports = SelectInput;
