var React = require('react');

var TemplateTile = React.createClass({

	render: function() {
    var template = this.props.template;
    var bgSrc = template.display_json.banner_background_url;

    var banner = {
      position: "relative",
      width: "100%",
      height: "100%",
      minHeight: bgSrc ? "inherit" : "60px",
      backgroundColor: bgSrc ? "transparent" : "#555"
    }

    var imageStyle = {
      display: "block",
      width: "100%",
    };

    var contentStyle = {
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      color: "#FFF",
      textAlign: "center"
    };

    return (      
      <li className={"template-tile " + (this.props.className || '') }
        onClick={this.props.onClick}>
        <span className="name">{ template.template_name }</span>
        <span className="type">({ template.display_json.type })</span>
        <div className="banner-image" style={banner}>
          <img style={imageStyle} src={bgSrc} />
          <div className='text-bg' style={contentStyle}>
            <span className="title">{ template.display_json.title }</span>
            <span className="desc">{ template.display_json.description }</span>
          </div>
        </div>
      </li>
     );
	},

});

module.exports = TemplateTile;