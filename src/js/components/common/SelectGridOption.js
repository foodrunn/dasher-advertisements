var React = require('react');

var component = React.createClass({

  // getInitialState: function() {
  //   return {
  //     selected: false
  //   };
  // },

  // componentWillMount: function() {
  //   this.setState({selected: this.props.selected});
  //   console.log(this.props.children, this.state.selected);
  // },

  // componentWillReceiveProps: function() {    
  //   this.componentWillMount();
  // },

  // toggleOption: function(evt) {
  //   this.setState({ selected: !this.state.selected });
  //   this.props.onChange(this.props.value, this.state.selected);
  // },

  render: function() {
    var css = 'select-grid-option' + (this.props.selected ? " selected" : "");

    return (
      <div className={css} value={this.props.value} onClick={this.props.toggleOption}>
        { this.props.children }
      </div>
    );  
  }
  
});

module.exports = component;
