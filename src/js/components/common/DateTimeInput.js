var React = require('react');
var DateTime = require('react-datetime');

var TextInput = React.createClass({

  getInitialState: function() {
    return {};
  },

  render: function() {
  	var that = this;

    var upload = (<div className="_no-upload" />);
    if (this.props.isUpload) {
      upload = this.renderUpload();
    }
    return (
      <span className="labeled-input">
        <label className={this.props.required ? " required" : ""}>{this.props.fieldName}</label>
        <DateTime 
          className={"output "+this.props.fieldName}
          dateFormat="YYYY-MM-DD"
          timeFormat="HH:mm"
          onChange={function(dt) {
          	// dt is a moment object if valid datetime, else string
          	if (typeof dt == 'string') {
          		return App.warn('Invalide date: '+dt);
          	}
          	that.props.handleChange(that.props.fieldName, dt.format('YYYY-MM-DD HH:mm'));
          }}
          value={this.props.value}
          name={this.props.fieldName} />
      </span>
    );
  }
});

module.exports = TextInput;
