var React = require('react');

var PromoTile = React.createClass({

	render: function() {
    var promo = this.props.promo;

    return (      
      <li className={"promo-tile " + this.props.className }
        onClick={this.props.onClick}>
        <div>
          <span className="title">{ promo.promo_name }</span>
          <span className="desc">{ promo.description }</span>
        </div>
      </li>
     );
	},

});

module.exports = PromoTile;