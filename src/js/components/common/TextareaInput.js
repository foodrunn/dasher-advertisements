var React = require('react');

var TextareaInput = React.createClass({

  getInitialState: function() {
    return {};
  },


  render: function() {
    return (
      <span className="labeled-input">
        <label className={this.props.required ? " required" : ""}>{this.props.fieldName}</label>
        <textarea className="output" name={this.props.fieldName} onChange={this.props.handleChange} rows="4" />
      </span>
    );
  }
});

module.exports = TextareaInput;
