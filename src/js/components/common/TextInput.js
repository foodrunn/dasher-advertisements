var React = require('react');

var TextInput = React.createClass({
  getInitialState: function() {
    return {
      enableUpload: false
    };
  },

  renderUpload: function() {
    return (
      <span className="upload-input">
        <form action="/api/s3upload" onSubmit={this.uploadFile} method="POST">
          <input type="file" name="image" onChange={this.validateFilename} />
          <button type="submit" disabled={!this.state.enableUpload}>
            Upload
          </button>
        </form>
        <span className="preview">
          <img
            src={this.getValidValue()}
            onClick={this.props.handlePreviewClick}
          />
        </span>
      </span>
    );
  },

  render: function() {
    var that = this;
    var upload = <div className="_no-upload" />;
    var change = this.props.handleChange;
    var link = this.props.link;
    if (this.props.isUpload) {
      upload = this.renderUpload();
      change = function(evt) {
        that.props.handleChange(evt);
        that.props.handlePreview(evt);
      };
    }
    return (
      <span className="labeled-input">
        <label className={this.props.required ? ' required' : ''}>
          {this.props.label !== undefined
            ? this.props.label
            : this.props.fieldName}
        </label>
        <input
          className="output"
          min={this.props.min}
          type={this.props.type || 'text'}
          onChange={change}
          value={this.getValidValue()}
          name={this.props.fieldName}
        />
        {upload}
        {link ? (
          <a href={link} target="_blank">
            {link}
          </a>
        ) : (
          ''
        )}
      </span>
    );
  },

  validateFilename: function(evt) {
    var filename = evt.target.value.split('\\').pop();
    var valid = filename.match(/^[A-Za-z-_\d\.]+$/);
    if (valid) {
      this.setState({ enableUpload: true });
    } else {
      App.warn(
        '<h3>Invalid File Name</h3> Please rename the file using only alphanumeric ' +
          'characters, hyphen, underscore, and period. No spaces or special characters.'
      );
    }
  },

  getValidValue: function() {
    return this.props.value === undefined ? '' : this.props.value;
  },

  uploadFile: function(evt) {
    var that = this;
    evt.preventDefault();
    var form = $(evt.target);
    var field = form.parents('.labeled-input').find('input.output');
    var button = form.find('button');
    var input = form.find('input');
    var file = input[0].files[0];

    if (!file) {
      return App.warn('Please choose a file');
    }
    button.html('Uploading...');

    // Check the file type.
    if (!file.type.match('image.*')) {
      return App.warn('File is not an image!');
    }

    // Create a new FormData object.
    var formData = new FormData();
    formData.append('image', file, file.name);

    // File restrictions
    if (this.props.uploadWidth)
      formData.append('width', this.props.uploadWidth);
    if (this.props.uploadHeight)
      formData.append('height', this.props.uploadHeight);
    if (this.props.uploadMaxSize)
      formData.append('maxSize', this.props.uploadMaxSize);

    $.ajax({
      url: form.attr('action'),
      type: 'POST',
      success: function(resp) {
        button.html('Upload');
        if (!resp.url) {
          return App.warn(
            'Unknown response from upload service. Maybe try again?'
          );
        }
        field.val(resp.url);
        that.props.handleChange({ target: field[0] });
      },
      error: function(err) {
        button.html('Upload');
        console.error(err);
        var msg = err.responseText ? err.responseText : '';
        App.warn('File upload error.<br/>' + msg);
      },
      // Form data
      data: formData,
      // Options to tell jQuery not to process data or worry about content-type.
      cache: false,
      contentType: false,
      processData: false
    });
  }
});

module.exports = TextInput;
