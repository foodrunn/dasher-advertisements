var React = require('react');

var Header = React.createClass({
  
  getInitialState: function() {
    return {
      mobileActive: false
    }  
  },


  renderOption: function(option) {
    var that = this;
    var active = location.hash.replace('#', '') == option.path ? 'active' : '';
    return (
      <li 
        className={ active }
        onClick={ function() {
          that.props.changeView(option.path);
          that.setState({ mobileActive: false });
        }} 
        key={option.name}>
        {option.name.toUpperCase()}
      </li>
    );
  },


  render: function() {
    var that = this;
    var options = [];
    options = this.props.menuOptions.map(this.renderOption);

    return (
      <div>
        <header className={ that.state.mobileActive ? 'mobile-active' : '' }>
          <div className="menu-link" onClick={ function() {that.setState({mobileActive:!that.state.mobileActive})} }>
            MENU
          </div>
          <ul>
            <li onClick={ ()=>{location.href='/';} }>{'< Back'}</li>
            {options}
            <li onClick={ ()=>{location.href='/logout';} }>{'Sign Out'}</li>
          </ul>
          <div className="env-right">
            [{ App.environment }]
          </div>
        </header>

        { this.props.children }
      </div>
    )
  }
});

module.exports = Header;
