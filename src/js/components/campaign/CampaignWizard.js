var React = require('react');
var moment = require('moment');
var CampaignFields = require('./CampaignFields');
var TemplateTile = require('../common/TemplateTile');
var PromoTile = require('../common/PromoTile');

var CampaignWizard = React.createClass({

  clientPath: function () {
    return App.clientApi() + 'v1/';
  },


  supportPath: function () {
    return App.supportApi() + 'v1/';
  },


  getInitialState: function () {
    var cj = {
      enabled: true,
      campaign_name: '',
      template_id: null,
      promo_program_id: null,
      position: {},
      overrides: [],
      campaign_calendars: [],
      campaign_conditions_map: [],
      containers: [],
      userfeed_container_id: ''
    };
    cj = this.populateStateFromExisting(cj)

    return {
      campaignId: null, // Existing campaign
      allTemplates: [],
      allPromos: [],
      allRegions: {},
      config: {},
      view: 'template',
      campaignJson: cj
    };
  },


  componentWillMount: function () {
    this.fetchEverything();
  },


  componentWillUnmount: function () {
    // Cancel any active network requests since the compnent is unmounting
    if (this.templateFetch) this.templateFetch.abort();
    if (this.promoFetch) this.promoFetch.abort();
    if (this.configFetch) this.configFetch.abort();
    if (this.regionFetch) this.regionFetch.abort();
    if (this.saveFetch) this.saveFetch.abort();
    if (this.deleteFetch) this.deleteFetch.abort();
  },


  componentWillUpdate: function (nextProps, nextState) {
    if (nextState.view != this.state.view) {
      window.scrollTo(0, 0);
    }
  },


  populateStateFromExisting: function (cj) {
    var existing = this.props.existingCampaign;
    if (existing) {
      cj.template_id = existing.template_id;
      cj.promo_program_id = existing.promo_program_id;
    }
    return cj;
  },


  fetchEverything: function () {
    this.templateFetch = this.getTemplates();
    this.promoFetch = this.getPromos();
    this.configFetch = this.getConfig();
    this.regionFetch = this.getRegions();
    this.userContainerFetch = this.getUserFeedContainers();
  },


  getTemplates: function () {
    var that = this;
    var path = this.supportPath() + 'marketing/templates';
    return App.ajax(path,
      function success(templates) {
        that.setState({ allTemplates: templates || [] });
      },
      function failure(err) {
        if (err.statusText === 'abort') return;
        console.error(err);
        App.warn('Unable to fetch existing templates.');
      }
    );
  },


  getPromos: function () {
    var that = this;
    var path = this.supportPath() + 'marketing/promotions';
    return App.ajax(path,
      function success(templates) {
        that.setState({ allPromos: templates || [] });
      },
      function failure(err) {
        if (err.statusText === 'abort') return;
        console.error(err);
        App.warn('Unable to fetch existing promotions.');
      }
    );
  },


  getConfig: function () {
    var that = this;
    var path = this.supportPath() + 'marketing/config';
    return App.ajax(path,
      function success(templates) {
        that.setState({ config: templates || [] });
      },
      function failure(err) {
        if (err.statusText === 'abort') return;
        console.error(err);
        App.warn('Unable to fetch config.');
      }
    );
  },


  getRegions: function () {
    var that = this;
    var path = this.clientPath() + 'app/config';
    return App.ajax(path,
      function success(data) {
        if (!data.regions) {
          if (err.statusText === 'abort') return;
          App.warn('Region fetch error.');
          return console.error('No regions!', data);
        }

        var newRegions = {};
        for (var r = 0; r < data.regions.length; r++) {
          var region = data.regions[r];
          var rName = region.official_name || region.name;
          newRegions[rName] = region.region_id;
        }
        that.setState({ allRegions: newRegions });
      },
      function failure(err) {
        if (err.statusText === 'abort') return;
        console.error('Region fetch failed:', err);
        App.warn('Unable to fetch regions.');
      }, true
    );
  },

  getUserFeedContainers: function () {
    var that = this;
    var path = this.supportPath() + 'marketing/userFeedContainers';
    return App.ajax(path,
      function success(containers) {
        that.setState({ userFeedContainers: containers || [] });
      },
      function failure(err) {
        if (err.statusText === 'abort') return;
        console.error(err);
        App.warn('Unable to fetch user feed containers.');
      }
    );
  },


  updateCampaignFields: function (fields) {
    const cj = Object.assign({}, this.state.campaignJson);
    for (let key in fields) {
      const val = fields[key];
      cj[key] = val;
    }
    delete cj.campaign_conditions; // Use the campaign_conditions_map instead
    this.setState({ campaignJson: cj });
    console.log('STATE', cj);
  },


  renderWizardView: function (items, prevView, nextView, nextEnabled) {
    var that = this;

    if (prevView)
      var backButton = (<div className="button" onClick={function () { prevView && that.setState({ view: prevView }) }}>Back</div>);
    if (nextView && nextEnabled)
      var nextButton = (<div className="button" onClick={function () { nextView && that.setState({ view: nextView }) }}>Next</div>);
    else
      var nextButton = (<div className="button disabled">Next</div>);

    return (
      <div className="overlay">
        <h3>{'Please choose a ' + this.state.view}</h3>
        <ul className="wizard-options">
          {items}
        </ul>
        <div className="wizard-buttons">
          {backButton}
          {nextButton}
        </div>
      </div>
    );
  },


  findTemplate: function (templateId) {
    return this.state.allTemplates.find((template) => {
      return template.template_id == templateId;
    });
  },


  findPromo: function (promoId) {
    return this.state.allPromos.find((promo) => {
      return promo.promo_id == promoId;
    });
  },


  render: function () {
    var that = this;

    function addTemplate(template, idx) {
      const templateId = that.state.campaignJson.template_id;
      var selected = templateId == template.template_id;
      var className = "wizard-option" + (selected ? ' selected' : '');
      return (
        <TemplateTile key={idx} className={className} template={template} onClick={() => {
          if (templateId == template.template_id) { // Deselect template
            var cj = Object.assign({}, that.state.campaignJson);
            cj.template_id = null;
          }
          else { // Select template
            var cj = Object.assign({}, that.state.campaignJson);
            cj.template_id = template.template_id;
          }
          that.setState({ campaignJson: cj })
        }} />
      );
    }
    const templates = this.state.allTemplates.map(addTemplate);

    function addPromo(promo, idx) {
      const promoId = that.state.campaignJson.promo_program_id;
      var selected = promoId == promo.promo_id;
      var className = "wizard-option" + (selected ? ' selected' : '');
      return (
        <PromoTile key={idx} className={className} promo={promo} onClick={() => {
          if (promoId == promo.promo_id) { // Deselect promo
            var cj = Object.assign({}, that.state.campaignJson);
            cj.promo_program_id = null;
          }
          else { // Select promo
            var cj = Object.assign({}, that.state.campaignJson);
            cj.promo_program_id = promo.promo_id;
          }
          that.setState({ campaignJson: cj })
        }} />
      );
    }
    const promos = this.state.allPromos.map(addPromo);

    var wizardView = null;
    switch (this.state.view) {
      case 'template':
        wizardView = this.renderWizardView(templates, '', 'promotion', this.state.campaignJson.template_id != null);
        break;
      case 'promotion':
        wizardView = this.renderWizardView(promos, 'template', 'fields', true);
        break;
      case 'fields':
        const template = this.findTemplate(this.state.campaignJson.template_id);
        const promotion = this.findPromo(this.state.campaignJson.promo_program_id);

        var existingCampaign = null;
        if (that.props.existingCampaign !== null) {
          existingCampaign = Object.assign({}, that.props.existingCampaign);
          existingCampaign.template_id = this.state.campaignJson.template_id;
          existingCampaign.promo_id = this.state.campaignJson.promo_id;
        }
        wizardView = (
          <div>
            <span className="link" onClick={() => { that.setState({ view: 'template' }) }}>
              Template: {template.template_name || 'Unnamed'}
            </span>
            <span className="link" onClick={() => { that.setState({ view: 'promotion' }) }}>
              Promotion: {promotion ? (promotion.promo_name || 'Unnamed') : 'NONE'}
            </span>
            <CampaignFields
              handleUpdateFields={this.updateCampaignFields}
              allRegions={this.state.allRegions}
              config={this.state.config}
              userFeedContainers={this.state.userFeedContainers}
              existingCampaign={existingCampaign} />
          </div>
        );
        break;
    }

    const existing = this.props.existingCampaign != null;
    let delButton = (<div />);
    if (existing) {
      delButton = (<div className="button delete" onClick={this.deleteCampaign}>Delete Campaign</div>);
    }
    const saveDisabled = this.state.view === 'fields' ? '' : ' disabled';
    return (
      <div className="wizard">
        {wizardView}
        <br />
        <div className={"button" + saveDisabled} onClick={this.saveCampaign}>
          {existing ? 'Update Campaign' : 'Save Campaign'}
        </div>
        {delButton}
      </div>);
  },


  saveCampaign: function () {
    const that = this;

    let data = this.state.campaignJson;
    delete data.campaign_id; // Don't want to send this to the API

    // Validate field
    let invalid = false;
    $('.output:visible').each(function () {
      let required = $(this).parents('.labeled-input').find('label.required').length != 0;
      if (required && !this.value) {
        invalid = this.name || '';
        return false;
      }
      else if (required && this.type == 'radio') { // Check all in radio group

        if (!$('input[type="radio"][name="' + this.name + '"]:checked').length) {
          invalid = this.name || '';
          return false;
        }
      }
      // Start date will be defaulted if blank
      else if ($(this).hasClass('start_date_time')) { // DateTime component doesn't support name attr
        const startInput = $(this).find('input');
        if (startInput.length && !startInput.val()) { // Blank
          const val = moment().format('YYYY-MM-DD HH:mm');
          data.campaign_calendars[0].start_date_time = val;
        }
      }
    });

    for (let g in data.campaign_conditions_map) {
      const group = data.campaign_conditions_map[g];

      for (let c = 0; c < group.length; c++) {
        const condition = group[c];
        const msg = this.conditionInvalid(condition);
        if (msg) {
          return App.warn('Invalid condition value.<br/>' + msg);
        }
        else if (condition.value.match(/^\d\d\d\d-\d\d-\d\d$/)) {
          condition.value += ' 00:00:00';
        }
      }
    }

    if (invalid !== false) {
      return App.warn('Please provide all required fields.<br/>' + invalid);
    }


    // Network request
    let path = this.supportPath() + 'marketing/campaigns';
    if (this.props.existingCampaign) {
      path += '/' + this.props.existingCampaign.campaign_id;
      var method = 'PUT';
    }
    this.saveFetch = App.ajax(path,
      function success(resp) {
        console.log(resp);
        App.notify('Campaign saved!');
        that.props.handleUpdateCampaigns();
      },
      function failure(err) {
        if (err.statusText === 'abort') return;
        console.error(err);
        App.warn(`Unable to save campaign. ${err.responseJSON.message}`);
      }, {
        method: method || 'POST',
        data: JSON.stringify(data)
      }
    );
  },


  conditionInvalid: function (condition) {
    switch (condition.type) {
      case 'user_created_date':
        const val = condition.value;
        // Must be a valid date (and maybe a time too)
        if (val && val.match(/(^\d\d\d\d-\d\d-\d\d$)|(^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d$)/)) {
          if (new Date(val) == "Invalid Date") {
            return 'Invalid date for user_created_date';
          }
        }
        else {
          return 'Invalid date for user_created_date';
        }
        break;
    }
    return false;
  },


  deleteCampaign: function () {
    if (!window.confirm('Comfirm delete?')) { return; }
    const that = this;

    let path = this.supportPath() + 'marketing/campaigns';
    if (this.props.existingCampaign) {
      path += '/' + this.props.existingCampaign.campaign_id;
    }
    else {
      return; // Shouldn't be able to happen...
    }
    this.deleteFetch = App.ajax(path,
      function success(resp) {
        console.log('DELETE', resp);
        App.notify('Campaign deleted successfully.');
        that.props.handleDelete();
      },
      function failure(err) {
        if (err.statusText === 'abort') return;
        console.error(err);
        App.warn('Unable to delete campaign.');
      },
      { method: 'DELETE' }
    );
  }
});

module.exports = CampaignWizard;
