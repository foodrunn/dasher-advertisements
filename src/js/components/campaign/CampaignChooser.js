var React = require('react');

var CampaignChooser = React.createClass({

  getInitialState: function() {
    return {
      conditionals: {},
    };
  },


  render: function() {
    var options = [];
    var disabledOpts = [];
    function addCampaign(campaign, idx) {
      var disabled = campaign.enabled == false;
      if (disabled)
        disabledOpts.push( (<option key={idx} value={campaign.campaign_id}>{campaign.campaign_name || 'NO NAME'}</option>) );
      else
        options.push( (<option key={idx} value={campaign.campaign_id}>{campaign.campaign_name || 'NO NAME'}</option>) );
    }
    this.props.allCampaigns.map(addCampaign);

    return (
      <section className="form">
        <p>Choose a campaign</p>
          <div className="select">
            <select onChange={this.props.handleChoice} name="all-campaign">
              <option value=''>Choose...</option>
              <optgroup label="Enabled">{ options }</optgroup>
              <optgroup label="Disabled">{ disabledOpts }</optgroup>
            </select>
          </div>
      </section>
    );
  }
});

module.exports = CampaignChooser;
