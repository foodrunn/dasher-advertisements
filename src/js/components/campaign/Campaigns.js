var React = require('react');
var CampaignWizard = require('./CampaignWizard');

var Campaigns = React.createClass({

  renderEditForm: function() {
    var existing = null;
    var chosenId = this.props.editCampaign;
    if (chosenId) {
      existing = this.props.allCampaigns.find(function(campaign){
        return campaign.campaign_id == chosenId;
      }, this);
    }

    return (
      <section>
        <p>Use this form to save a marketing campaign.</p>
        <div className={ 'banner' + (chosenId ? ' alert' : '') }>
          { chosenId ? 'Editing Campaign '+chosenId+'.' : 'Editing New Campaign' }
        </div>
        <div className="form">
          <CampaignWizard 
            handleUpdateCampaigns={ this.handleCampaignUpdate }
            existingCampaign={ existing }
            handleDelete={ this.props.handleDeleteCampaign } />
        </div>
      </section>
    );
  },


  render: function() {
    return ( 
      <section className="campaign-wrapper">
        <h2>Create/Update Marketing Campaigns</h2>
          { this.renderEditForm() }
      </section>
    );
  },


  // Only fires on success
  handleCampaignUpdate: function() {
    this.props.onUpdateCampaign();    
  }
});

module.exports = Campaigns;
