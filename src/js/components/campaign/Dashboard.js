var React = require('react');
var moment = require('moment');
var DashboardTable = require('./DashboardTable');
var Campaigns = require('./Campaigns');

var Dashboard = React.createClass({

  clientPath: function() {
    return App.clientApi() + 'v1/';
  },


  supportPath: function() {
    return App.supportApi() + 'v1/';
  },


  getInitialState: function() {
    return {
      loading: true,
      view: 'dashboard',
      editCampaign: null,
      allCampaigns: [],
      allTemplates: [],
      allRegions: {},
      regionTimes: {},
      allPromos: []
    };
  },


  componentDidMount: function() {
    this.fetchEverything();
  },


  fetchEverything: function() {
    var that = this;
    var toLoad = 4;
    var loaded = 0;
    var allCampaigns,
        allTemplates,
        allPromos,
        allRegions,
        regionTimes;

    // Only set state once everything has loaded.
    // No thrashing/weird in-between states
    function updateLoadCount() {
      if (++loaded >= toLoad) {
        that.setState({
          allCampaigns: allCampaigns,
          allTemplates: allTemplates,
          allPromos: allPromos,
          allRegions: allRegions,
          regionTimes: regionTimes,
          loading: false
        });
      }
    }

    // Fetch list of existing campaigns
    var path = this.supportPath() + 'marketing/campaigns';
    App.ajax(path,
      function success(campaigns) {
        allCampaigns = campaigns;
        updateLoadCount();
      },
      function failure(err) {
        console.error(err);
        App.warn('Unable to fetch existing campaigns.');
      }
    );

    // Fetch list of existing template
    path = this.supportPath() + 'marketing/templates';
    App.ajax(path,
      function success(templates) {
        allTemplates = templates;
        updateLoadCount();
      },
      function failure(err) {
        console.error(err);
        App.warn('Unable to fetch existing templates.');
      }
    );

    // Fetch Promotions
    path = this.supportPath() + 'marketing/promotions';
    App.ajax(path,
      function success(promos) {
        allPromos = promos;
        updateLoadCount();
      },
      function failure(err) {
        console.error(err);
        App.warn('Unable to fetch existing promotions.');
      }
    );

    // Fetch Regions
    path = this.clientPath() + 'app/config';
    App.ajax(path,
      function success(data) {
        if (!data.regions) {
          App.warn('Region fetch error.');
          return console.error('No regions!', data);
        }

        var newRegions = {0: 'All'};
        var newRegionTimes = {};
        for (var r=0; r<data.regions.length; r++) {
          var region = data.regions[r];
          newRegions[region.region_id] = region.name;
          newRegionTimes[region.region_id] = region.timezone;
        }
        allRegions = newRegions;
        regionTimes = newRegionTimes;
        updateLoadCount();
      },
      function failure(err) {
        console.error('Region fetch failed:', err);
        App.warn('Unable to fetch regions.');
      }, true
    );
  },


  getActiveCampaigns: function(campaign) {
    try {
      var cal = campaign.campaign_calendars[0];
    } catch(err) {
      console.warn('Bad Campaign. ID:', campaign.campaign_id);
      return false;
    }
    var isNew = false;
    var now = moment();
    var start = moment(cal.start_date_time);
    var end = moment(cal.end_date_time);
    if (end > now &&    // Hasn't ended
        now > start) {  // Started already
      isNew = true;
    }
    // return campaign.enabled == true && isNew;
    return true; // Don't filter here (at least for now...)
  },


  appendTemplate: function(campaign) {
    var withTemplate = campaign;
    withTemplate.templateObj = this.state.allTemplates.find((template) => {
      return campaign.template_id == template.template_id;
    });
    return withTemplate;
  },


  render: function() {
    var that = this;

    if (this.state.loading) {
      return <div>Loading...</div>
    }
    
    var button;
    var content;
    switch (this.state.view) {
      case 'dashboard':
        button = (
          <div className="button create" onClick={()=>{that.setState({view:'edit', editCampaign: null})}} >
            New Campaign
          </div>
        );
        content = this.renderDashboard();
        break;
      case 'edit':
        button = (
          <div className="button cancel" onClick={()=>{that.setState({view:'dashboard', editCampaign: null})}} >
            Cancel
          </div>
        );
        content = this.renderEdit();
        break;
    };

    return (
      <div>
        <div className="campaign-buttons form">
          { button }
        </div>
          { content }
      </div>
    );
  },


  renderDashboard: function() {
    try {
      return (<DashboardTable
        regions={this.state.allRegions}
        timezones={this.state.regionTimes}
        promos={this.state.allPromos}
        campaigns={this.state.allCampaigns.filter(this.getActiveCampaigns).map(this.appendTemplate)}
        handleEditCampaign={this.handleEditCampaign} />
      );
    } catch(err) {
      console.error(err);
      App.warn('Invalid Campaign. Please seek help.');
      return (<div>CAMPAIGN ERROR</div>);
    }
  },


  renderEdit: function() {
    return (<Campaigns 
      editCampaign={this.state.editCampaign}
      allCampaigns={this.state.allCampaigns}
      onUpdateCampaign={this.handleUpdateCampaign}
      handleDeleteCampaign={this.handleDeleteCampaign} />);
  },


  handleEditCampaign: function(campaignId) {
    this.setState({
      view: 'edit',
      editCampaign: campaignId
    });
  },


  handleUpdateCampaign: function() {
    console.log('Campaign updated.');
    this.setState({
      view:'dashboard',
      editCampaign: null
    }, this.fetchEverything);
  },


  handleDeleteCampaign: function() {
    this.setState({view: 'dashboard'}, this.fetchEverything);
  }
});

module.exports = Dashboard;
