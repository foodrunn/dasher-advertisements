var React = require('react');
var BooleanInput = require('../common/BooleanInput');
var SelectInput = require('../common/SelectInput');
var SelectGrid = require('../common/SelectGrid');
var TextInput = require('../common/TextInput');
var DateTimeInput = require('../common/DateTimeInput');
var moment = require('moment');

var CampaignFields = React.createClass({
  getInitialState: function() {
    return {
      overrideCount: 0,
      conditionCount: 0,
      position: {},
      calendars: [],
      overrides: [],
      conditions: [],
      showAdvanced: false,
      formData: this.resetForm()
    };
  },

  componentDidMount: function() {
    var that = this;
    if (this.props.existingCampaign) {
      var existing = Object.assign({}, this.props.existingCampaign);
      if (!existing.campaign_conditions_map) {
        existing.campaign_conditions_map = {};
      }
      this.setState({ formData: existing }, () => {
        that.props.handleUpdateFields(this.state.formData);
      });
    }
  },

  componentWillReceiveProps: function(nextProps) {
    if (this.props.existingCampaign && !nextProps.existingCampaign) {
      this.setState({ formData: this.resetForm() });
    }
  },

  resetForm: function() {
    return {
      campaign_name: '',
      position: { type: '', index: '' },
      campaign_calendars: [
        { regions: [], start_date_time: '', end_date_time: '' }
      ],
      campaign_conditions: [],
      campaign_conditions_map: {},
      overrides: [],
      userfeed_container_id: ''
    };
  },

  addConditionGroup: function() {
    const formData = Object.assign({}, this.state.formData);
    const groups = Object.assign({}, formData.campaign_conditions_map);
    const len = Object.keys(groups).length;
    const newKey = -1 * (len + 1);
    groups[newKey] = [{}];
    formData.campaign_conditions_map = groups;
    this.setState({ formData });
  },

  renderOverrides: function() {
    var that = this;
    var newFormData = that.state.formData;

    function popOverride() {
      newFormData.overrides.pop();
      that.setState({ formData: newFormData });
    }
    var overrides = [];
    for (var o = 0; o < that.state.formData.overrides.length; o++) {
      var close =
        o == that.state.formData.overrides.length - 1 ? (
          <div className="close" onClick={popOverride}>
            &#10006;
          </div>
        ) : (
          <div />
        );

      var data = that.state.formData.overrides[o];
      overrides.push(
        <div key={o} className="group override" data-pos={o}>
          {close}
          <TextInput
            fieldName="field_name"
            value={data.field_name || ''}
            handleChange={that.updateOverrides}
            required={true}
            required={true}
          />
          <TextInput
            fieldName="field_value"
            value={data.field_value || ''}
            handleChange={that.updateOverrides}
            required={false}
          />
        </div>
      );
    }
    return <div>{overrides}</div>;
  },

  renderContainers: function() {
    const formData = this.state.formData;

    var containers = this.props.userFeedContainers;
    var feedContainers = [];
    var that = this;

    containers.map(function(container, idx) {
      feedContainers.push(
        <option key={idx} value={container.id}>
          {container.label}
        </option>
      );
    });

    feedContainers.splice(
      0,
      0,
      <option key="nada" value="">
        Choose an option
      </option>
    );
    return (
      <select
        value={formData.userfeed_container_id}
        className="output"
        onChange={this.updateUserFeedContainer}
      >
        {feedContainers}
      </select>
    );
  },

  renderConditions: function(conditionGroup, groupKey) {
    const removeCondition = index => {
      const formData = Object.assign({}, this.state.formData);
      const groups = Object.assign({}, formData.campaign_conditions_map);
      const group = groups[groupKey].slice();
      group.splice(index, 1);
      groups[groupKey] = group;

      if (group.length === 0) {
        delete groups[groupKey];
      }
      formData.campaign_conditions_map = groups;
      this.setState({ formData });
    };

    const conditions = [];
    const types = this.props.config.condition_types;
    const operators = this.props.config.operators;

    for (let c = 0; c < conditionGroup.length; c++) {
      const close = (
        <div className="close" onClick={() => removeCondition(c)}>
          &#10006;
        </div>
      );
      const condition = conditionGroup[c];
      const showAdvanced = this.state.showAdvanced === c + groupKey;
      const hasleftOperand =
        (condition.left_operand !== undefined &&
          condition.left_operand !== null &&
          condition.left_operand !== '') ||
        (condition.left_operand_2 !== undefined &&
          condition.left_operand_2 !== null &&
          condition.left_operand_2 !== '');

      conditions.push(
        <div key={c} className="condition" data-pos={c}>
          {close}
          <SelectInput
            fieldName="type"
            value={condition.type}
            opts={types}
            handleChange={evt =>
              this.updateConditions(groupKey, c, 'type', evt.target.value)
            }
            required={true}
          />
          <SelectInput
            fieldName="operator"
            value={condition.operator}
            opts={operators}
            handleChange={evt =>
              this.updateConditions(groupKey, c, 'operator', evt.target.value)
            }
            required={true}
          />
          <TextInput
            fieldName="value"
            value={condition.value || ''}
            handleChange={evt =>
              this.updateConditions(groupKey, c, 'value', evt.target.value)
            }
            required={true}
          />
          {(showAdvanced || hasleftOperand) && (
            <span>
              <TextInput
                fieldName="left_operand"
                value={condition.left_operand || ''}
                handleChange={evt =>
                  this.updateConditions(
                    groupKey,
                    c,
                    'left_operand',
                    evt.target.value
                  )
                }
                required={false}
              />
              <TextInput
                fieldName="left_operand_2"
                value={condition.left_operand_2 || ''}
                handleChange={evt =>
                  this.updateConditions(
                    groupKey,
                    c,
                    'left_operand_2',
                    evt.target.value
                  )
                }
                required={false}
              />
            </span>
          )}
          {!hasleftOperand && (
            <span
              className="link"
              onClick={() =>
                this.setState({
                  showAdvanced:
                    this.state.showAdvanced !== false ? false : c + groupKey
                })
              }
            >
              {this.state.showAdvanced === c + groupKey ? 'Hide' : 'Show'}{' '}
              Advanced
            </span>
          )}
        </div>
      );
    }
    return <div>{conditions}</div>;
  },

  render: function() {
    const addOverride = () => {
      var newFormData = this.state.formData;
      newFormData.overrides.push({});
      this.setState({ formData: newFormData });
    };

    const formData = this.state.formData;

    return (
      <div className="campaign-fields">
        <div className="single-field">
          <TextInput
            fieldName="campaign_name"
            required={true}
            value={formData.campaign_name || ''}
            handleChange={this.updateField}
          />
        </div>

        <div className="field-group dates" name="campaign_calendars">
          <label>Calendar</label>
          <div className="group" data-pos="0">
            <DateTimeInput
              fieldName="start_date_time"
              value={formData.campaign_calendars[0].start_date_time}
              handleChange={this.updateCalendars}
            />
            <DateTimeInput
              fieldName="end_date_time"
              value={formData.campaign_calendars[0].end_date_time}
              handleChange={this.setEndOfDay}
            />
            <br />
            <SelectGrid
              fieldName="regions"
              includeAll="true"
              sortBy="name"
              value={formData.campaign_calendars[0].regions}
              opts={this.props.allRegions}
              required={true}
              handleChange={this.updateCalendars}
            />
          </div>
        </div>

        <div className="field-group position" name="position">
          <label>Position</label>
          <div className="group">
            <SelectInput
              fieldName="type"
              required={true}
              value={formData.position.type}
              opts={['fixed', 'floating']}
              handleChange={this.updatePosition}
            />
            <TextInput
              type="number"
              min="0"
              fieldName="index"
              required={true}
              value={formData.position.index}
              handleChange={this.updatePosition}
            />
          </div>
        </div>

        <div
          className="field-group user-feed-container"
          name="user_feed_container"
        >
          <label>User feed</label>
          <div className="group">
            <span className="labeled-input">
              <label className="feed-container">feed container</label>
              <div className="select">{this.renderContainers()}</div>
            </span>
          </div>
        </div>

        <div className="field-group overrides" name="overrides">
          <label>Overrides</label>
          {this.renderOverrides()}
          <button onClick={addOverride}>Add Override</button>
        </div>

        <div className="field-group conditions" name="campaign_conditions">
          <label>Conditions</label>

          {Object.keys(this.state.formData.campaign_conditions_map).map(
            (key, i) => {
              const group = this.state.formData.campaign_conditions_map[key];
              return (
                <div key={i} className="cond-group group">
                  {this.renderConditions(group, key)}
                  <button
                    disabled={!this.canAddCondition(key)}
                    onClick={() => this.addCondition(key)}
                  >
                    Add Condition
                  </button>
                </div>
              );
            }
          )}
          <button
            disabled={!this.canAddConditionGroup()}
            onClick={this.addConditionGroup}
          >
            + Add Group
          </button>
        </div>
      </div>
    );
  },

  addCondition: function(groupKey) {
    var formData = Object.assign({}, this.state.formData);
    formData.campaign_conditions_map[groupKey].push({});
    this.setState({ formData });
  },

  canAddConditionGroup: function() {
    let ret = true;
    const groups = this.state.formData.campaign_conditions_map;
    for (let key in groups) {
      const condition = groups[key];
      if (!condition.length || (condition.length && !condition[0].type)) {
        ret = false;
      }
    }
    return ret;
  },

  canAddCondition: function(groupKey) {
    let ret = true;
    const group = this.state.formData.campaign_conditions_map[groupKey];
    for (let condition of group) {
      if (Object.keys(condition).length === 0) {
        return false;
      }
    }
    return ret;
  },

  updateField: function(evt) {
    var newFormData = this.state.formData;
    newFormData[evt.target.name] = $(evt.target).val();
    this.setState({ formData: newFormData });
    this.props.handleUpdateFields(newFormData);
  },

  updatePosition: function(evt) {
    // Assumes only one calendar for this campaign
    var newFormData = this.state.formData;
    let positionValue = $(evt.target).val();

    if (positionValue.includes('-')) {
      const msg = `Index value can't be negative. Setting a positive value.`;
      App.warn(msg);
      positionValue = positionValue.replace('-', '');
    }

    newFormData.position[evt.target.name] = positionValue;
    this.setState({ formData: newFormData });
    this.props.handleUpdateFields(newFormData);
  },

  setEndOfDay: function(field, val) {
    if (val.includes('00:00')) {
      console.warn('Setting midnight (00:00) to end of day (23:59).');
      val = val.replace('00:00', '23:59');
    }
    this.updateCalendars(field, val);
  },

  updateCalendars: function(field, val) {
    if (typeof field !== 'string') {
      // DateTime component sends string value, this is regions
      var evt = field;
      field = evt.target.name;
      val = $(evt.target).val();
    }
    var newFormData = this.state.formData;
    var obj = newFormData.campaign_calendars[0] || {};
    obj[field] = val;
    newFormData.campaign_calendars = [obj];
    this.setState({ formData: newFormData });
    this.props.handleUpdateFields(newFormData);
  },

  updateOverrides: function(evt) {
    var group = $(evt.target).parents('.group');
    var pos = group.attr('data-pos');
    var newFormData = this.state.formData;
    var ovr = newFormData.overrides;
    var obj = ovr[pos] || {};
    // Allow blank value
    if (evt.target.name == 'field_name') {
      obj['field_value'] = group.find('.output[name="field_value"]').val();
    }
    obj[evt.target.name] = evt.target.value;
    ovr[pos] = obj;
    newFormData.overrides = ovr;
    this.setState({ formData: newFormData });
    this.props.handleUpdateFields(newFormData);
  },

  updateConditions: function(groupKey, index, field, val) {
    const formData = Object.assign({}, this.state.formData);
    const group = formData.campaign_conditions_map[groupKey].slice();
    const cond = group[index] || {};
    cond[field] = val;
    group[index] = cond;

    formData.campaign_conditions_map[groupKey] = group;
    this.setState({ formData });
    this.props.handleUpdateFields(formData);
  },

  updateUserFeedContainer: function(evt) {
    var newFormData = this.state.formData;
    var feedId = parseInt($(evt.target).val());

    newFormData.userfeed_container_id = feedId;

    this.setState({ formData: newFormData });
    this.props.handleUpdateFields(newFormData);
  }
});

module.exports = CampaignFields;
