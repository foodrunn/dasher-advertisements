var React = require('react');
var ReactBsTable  = require('react-bootstrap-table');
var BootstrapTable = ReactBsTable.BootstrapTable;
var TableHeaderColumn = ReactBsTable.TableHeaderColumn;
var moment = require('moment-timezone');
var TemplateTile = require('../common/TemplateTile');

var DashboardTable = React.createClass({

	getInitialState: function() {
		return {
			smokeOn: false,
			hideExpired: true
		};
	},


	handleRowClick: function(row) {
		this.props.handleEditCampaign(row.id);
	},


	findCampaign: function(cid) {
		return this.props.campaigns.find(function(c) {
			return c.campaign_id == cid;
		});
	},


	renderTemplate: function(templateType, row) {
		var template = row.template;
		if (!template) {
			return (<div className="error">MISSING TEMPLATE</div>);
		}
		return ( <TemplateTile template={template} onClick={this.toggleExpandTemplate} /> );
	},


	renderPromo: function(promo, row) {
		if (promo === 'NONE') {
			return promo;
		}
		return (<span className="bubble">{promo}</span>);
	},


	renderDateTime: function(field, dateTime) {
		if (!dateTime) {
			if (field === 'start_date_time')
				console.error('Missing start_date_time');
			return (<span className="date bubble">NO END</span>);
		}
		return moment(dateTime).format('ddd, MMM Do YYYY, h:mma')
	},


	renderStartTime: function(startTime, row) {
		return this.renderDateTime('start_date_time', startTime);
	},


	renderEndTime: function(endTime, row) {
		return this.renderDateTime('end_date_time', endTime);
	},


	render: function() {
    var options = {
      onRowClick: this.handleRowClick
    };
    const statusEnum = {
		  0: 'DISABLED',
		  1: 'PAST',
		  2: 'FUTURE',
		  3: 'LIVE'
		};
		function statusEnumFormatter(cell, row, enumObject) {
		  return enumObject[cell];
		}
    var regexFilter = {type: 'RegexFilter', delay: 600};
    var statusFilter = {type: 'SelectFilter', options: statusEnum};
    var campaignRows = this.formatCampaignRows();

    return (
    	<div>
    		<label className="expired-toggle">Hide Expired
    			<input type="checkbox" checked={this.state.hideExpired} onChange={evt => this.setState({hideExpired: evt.target.checked})} />
    		</label>
	      <BootstrapTable data={campaignRows} options={options} striped hover>
	        <TableHeaderColumn isKey dataField='name' filter={ regexFilter } >Name</TableHeaderColumn>
	        <TableHeaderColumn dataField='status' filter={ statusFilter }  filterFormatted dataFormat={ statusEnumFormatter } formatExtraData={ statusEnum }>Status</TableHeaderColumn>
	        <TableHeaderColumn dataField='templateType' filter={ regexFilter } dataFormat={ this.renderTemplate } >Template (type)</TableHeaderColumn>
	        <TableHeaderColumn dataField='promo' filter={ regexFilter } dataFormat={ this.renderPromo } >Promotion</TableHeaderColumn>
	        <TableHeaderColumn dataField='regions' filter={ regexFilter } >Regions</TableHeaderColumn>
	        <TableHeaderColumn dataField='timezone' filter={ regexFilter } >Timezone</TableHeaderColumn>
	        <TableHeaderColumn dataField='startDate' dataFormat={ this.renderStartTime } dataSort={ true }>Start (local)</TableHeaderColumn>
	        <TableHeaderColumn dataField='endDate' dataFormat={ this.renderEndTime } dataSort={ true }>End (local)</TableHeaderColumn>
	      </BootstrapTable>

	      <div className="smoke-screen" style={{display: this.state.smokeOn ? 'block' : 'none'}}
	      	onClick={this.toggleExpandTemplate} />
	     </div>
     );
	},


	toggleExpandTemplate: function(evt) {
		evt.stopPropagation();
		var template = $('li.template-tile.expanded');
		template = template.length ? template : $(evt.currentTarget);
		template.toggleClass('expanded');
		this.setState({smokeOn: !this.state.smokeOn});
	},


	formatCampaignRows: function() {
		var that = this;
		var formatted = [];
		var campaigns = this.props.campaigns;

		function formatFields(campaign) {
			var templateObj = campaign.templateObj;
			return {
				id: campaign.campaign_id,
				name: campaign.campaign_name,
				status: that.setStatus(campaign),
				promo: that.setPromo(campaign),
				template: templateObj,
				templateType: templateObj ? templateObj.display_json.type : '',
				regions: that.setRegions(campaign),
				timezone: campaign.timezone,
				startDate: that.setStartDate(campaign),
				endDate: that.setEndDate(campaign)
			};
		}

		// One row per timezone per the attached regions
		for (var c=0; c<campaigns.length; c++) {
			var campaign = campaigns[c];
			var regions = campaign.campaign_calendars[0].regions;
			var zones = {};

			// Hide expired campaigns if the box is checked
			const end = campaign.campaign_calendars[0].end_date_time;
			if (this.state.hideExpired && end) {
				if (moment().isAfter(moment(end))) {
					continue;
				}
			}

			// Each zone has an array of region IDs
			for (var r=0; r<regions.length; r++) {
				var regionId = regions[r];
				var tz = this.props.timezones[regionId];
				tz = regionId == 0 ? "ALL" : tz;
				if (!tz) {
					continue; // Region is invalid
				}
				var regionArray = zones[tz] || [];
				regionArray.push(regionId);
				zones[tz] = regionArray;
			}
			// One row per zone
			for (var zone in zones) {
				var newC = $.extend(true, {}, campaign); // Objects are pass-by-refrence, duh
				newC.timezone = zone;
				newC.campaign_calendars[0].regions = zones[zone]; // Regions in the zone
				formatted.push( formatFields(newC) );
			}
		}
		return formatted;
	},


	setStatus: function(campaign) {
		var hereNow = moment();
		if (campaign.enabled == false || campaign.enabled == 'false') {
			return 0; // DISABLED
		}
    var cal = campaign.campaign_calendars[0];
    var tz = campaign.timezone;
    if (tz && tz != 'ALL')
    	var localNow = moment().tz(tz);
    else
    	var localNow = moment();

    // Convert other time to this time for a fair comparison
    var hereThere = moment(localNow.format('YYYY-MM-DD HH:mm'));

    var start = moment(cal.start_date_time);
    var end = moment(cal.end_date_time);
    if (hereThere > end) {
    	return 1; // PAST
    }
    if (hereThere < start) {
    	return 2; // FUTURE
    }
    return 3; // LIVE
	},


	setPromo: function(campaign) {
		var promotion = this.props.promos.find((promo) => {
			return promo.promo_id == campaign.promo_program_id;
		});
		if (promotion) {
			return promotion.promo_name;
		}
		return 'NONE';
	},


	setRegions: function(campaign) {
		return campaign.campaign_calendars[0].regions.map((rid) => {
			return this.props.regions[rid];
		}).join(', ');
	},


	setDate: function(campaign, field) {
		var dateTime = campaign.campaign_calendars[0][field];
		if (!dateTime) {
			return '';
		}
		return new Date(dateTime);
	},


	setStartDate: function(campaign) {
		return this.setDate(campaign, 'start_date_time');
	},


	setEndDate: function(campaign) {
		return this.setDate(campaign, 'end_date_time');
	}
});

module.exports = DashboardTable;