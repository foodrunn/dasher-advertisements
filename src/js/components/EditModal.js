var React = require('react');
var Modal = require('react-modal');

const modalStyle = {
  content: {
    top        : '50%',
    left       : '50%',
    right      : 'auto',
    bottom     : 'auto',
    marginRight: '-50%',
    transform  : 'translate(-50%, -50%)'
  }
};

var EditModal = React.createClass({

  basePath: function() {
    return App.supportApi() + 'v1/';
  },


  getInitialState: function() {
    return {
    	newRoles: [],
    	newStatus: '',
    };
  },


  componentWillReceiveProps: function(nextProps) {
  	if (nextProps.modalIsOpen) {
  		this.setState({
  			newRoles: nextProps.editUser.roles,
  			newStatus: nextProps.editUser.status
  		});
  	}
  },


  setNewRoles: function(evt) {
    var newRoles = [];
    $('.role-box input[type="checkbox"]').each(function(el) {
      if ( $(this).is(':checked') )
        newRoles.push($(this).val());
    });
    this.setState({newRoles: newRoles});
  },


  setNewStatus: function(evt) {
    this.setState({newStatus: evt.target.value});
  },


	render: function() {
    var editUser = this.props.editUser;

    if (!editUser)
      return (<div />);

    var that=this;

    function addRoleBox(role, idx) {
      var roleChecked = false;
      if (that.state.newRoles.indexOf(role) >= 0) {
        roleChecked = true;
      }
      return (
        <span key={idx} className="role-box">
          <label>
            <input 
              type="checkbox"
              disabled={ role === 'user' }
              value={ role } 
              checked={ roleChecked } 
              onChange={ that.setNewRoles }/>
            {role}
          </label>
        </span>);
    }

    function addStatusRadio(status, idx) {
      var radioChecked = false;
      if (that.state.newStatus == status) {
        radioChecked = true;
      }
      return (
        <span key={idx} className="role-box">
          <label>
            <input 
              type="radio" 
              name="user-status" 
              value={ status } 
              checked={radioChecked} 
              onChange={ that.setNewStatus} />
            { status }
          </label>
        </span>);
    }

    var roleBoxes = this.props.rolesEnum.map(addRoleBox);
    var statusRadios = this.props.statusesEnum.map(addStatusRadio);

    return (
      <Modal
          isOpen={this.props.modalIsOpen}
          onRequestClose={this.props.handleCloseModal}
          contentLabel='foo'
          style={modalStyle}
          >
        <h3>{editUser.display_name}</h3>
        <div className="content">
          <div className="heading">User Roles:</div>
          <div>
            { roleBoxes }
          </div>

          <div className="heading">User Status:</div>
          <div>
            { statusRadios }
          </div>`
        </div>

        <div className="button button-cancel" onClick={ this.props.handleCloseModal }>Cancel</div>
        <div className="button button-submit" onClick={ function(){that.submitChanges()} }>Submit</div>
      </Modal>
    );
  },


  submitChanges: function() {
    var roleRequest = { "user_role_map": [{
      "dasher_user_id": this.props.editUser.dasher_user_id,           
      "roles": this.state.newRoles
    }]};
    this.putRoles(roleRequest);

    var statusRequest = {"user_status_list": [{
      "dasher_user_id": this.props.editUser.dasher_user_id,
      "user_status": this.state.newStatus
    }]};
    this.putStatus(statusRequest);
    
    this.props.handleCloseModal();
  },


  put: function(apiPath, data, success, failure) {
    console.log('PUT: ', this.basePath() + apiPath);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'PUT',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: success,
      error: failure
    });
  },



  putRoles: function(json) {
    var path = 'dashboard/users/roles';
    console.log('PUT roles', json);

    this.put(path, json, 
      function success(resp) {
        console.log(resp);
      },
      function failure(err) {
        console.error('PUT failed');
        App.warn('Save failed. 101');
      }
    );
  },


  putStatus: function(json) {
    var path = 'dashboard/users/status';
    console.log('PUT status', json);

    this.put(path, json,
      function success(resp) {
        console.log(resp);
      },
      function failure(err) {
        console.error('PUT failed');
        App.warn('Save failed. 110');
      }
    );
  }
});

module.exports = EditModal;
